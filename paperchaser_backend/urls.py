"""paperchaser_backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
import sys

import os
from django.conf.urls import url, include
from django.contrib import admin
from rest_framework.schemas import get_schema_view
from rest_framework.urlpatterns import format_suffix_patterns

from paperchaser_backend.apps.posts.views import PostListView, PostDetailView

RESTAPIpatterns = [
    url(r'^posts/$', PostListView.as_view(), name='posts'),
    url(r'^posts/(?P<uuid>[^/]+)/$', PostDetailView.as_view(), name='post'),
]

urlpatterns = [
    url(r'^api/', include(RESTAPIpatterns)),
    url(r'^schema/', get_schema_view(title="Server Monitoring API")),
    url(r'^s45GFk12/', admin.site.urls),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]

urlpatterns = format_suffix_patterns(urlpatterns)
