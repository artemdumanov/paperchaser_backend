from django.contrib import admin
from .models import Post


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ('publish_date', 'public', 'tags', 'text')
    list_filter = ('publish_date', 'public')

    def tags(self, obj):
            return obj.tags

    tags.empty_value_display = "['']"
