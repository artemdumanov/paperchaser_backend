from rest_framework import permissions, generics

from paperchaser_backend.apps.posts.models import Post
from paperchaser_backend.apps.posts.serializers import PostExtendedSerializer


class PostListView(generics.ListAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = PostExtendedSerializer

    def get_queryset(self):
        return Post.objects.filter(public=True)


class PostDetailView(generics.RetrieveAPIView):
    lookup_field = 'uuid'
    permission_classes = (permissions.AllowAny,)
    queryset = Post.objects.all()
    serializer_class = PostExtendedSerializer
