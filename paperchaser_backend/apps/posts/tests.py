import json

from django.db import connection
from django.test import TestCase
from rest_framework.test import RequestsClient

from .models import Post, ListFieldValueException


class PostModelTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.post = Post.objects.create(tags=['tag1', 'tag2', 'tag3'],
                                       text='Test text',
                                       public=True)

    def test_post_tags(self):
        assert self.post.tags == ['tag1', 'tag2', 'tag3']

    def test_post_tag_five_length(self):
        Post.objects.create(tags=['tag1', 'tag2', 'tag3', 'tag4', 'tag5'],
                            text='Test text',
                            public=True)

    def test_post_str_list_tags(self):
        Post.objects.create(tags="['tag1', 'tag2', 'tag3', 'tag4', 'tag5']",
                            text='Test text',
                            public=True)

    def test_post_str_tags(self):
        Post.objects.create(tags="tag1, tag2, tag3, tag4, tag5'",
                            text='Test text',
                            public=True)

    # def test_post_no_tags(self):
    #     with self.assertRaises(ListFieldValueException):
    #         print(Post.objects.create(text='Test text').tags)

    def test_post_tags_more_five_length(self):
        with self.assertRaises(ListFieldValueException):
            Post.objects.create(tags=['tag1', 'tag2', 'tag3', 'tag4', 'tag5', 'tag6'],
                                text='Test text',
                                public=True)

    # def test_invalid_tags(self):
    #     with connection.cursor() as cursor:
    #         cursor.execute("INSERT INTO test_papaerchaser.post VALUES ")

class PostDataBaseTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.client = RequestsClient()
        cls.posts = [
            Post.objects.create(tags=['tag1', 'tag2', 'tag3'],
                                text='Test text 1',
                                public=True),
            Post.objects.create(tags=['tag3', 'tag2', 'tag4'],
                                text='Test text 2',
                                public=True),
            Post.objects.create(tags=['tag1', 'tag3', 'tag6'],
                                text='Test text 3',
                                public=True)
        ]


class PostListDataViewTestCase(PostDataBaseTestCase):
    def test_get_not_empty_post_list(self):
        request = self.client.get('/api/posts/')

        assert request.status_code == 200 and request.json() != {}

    # def test_post_posts(self):
    #     data = {'text': 'Super new post',
    #             'tags': ['supertags', 'onemore']}
    #     request = self.client.post('/api/posts/', data)
    #
    #     assert request.status_code == 201


empty_response = {'count': 0, 'next': None,
                  'previous': None, 'results': []}


class PostEmptyListViewTestCase(TestCase):
    def test_empty_post_list(self):
        request = self.client.get('/api/posts/')

        assert request.status_code == 200 and request.json() == empty_response


class PostDetailDataViewTestCase(PostDataBaseTestCase):
    def setUp(self):
        self.post = self.posts[0]

    def test_get_post(self):
        request = self.client.get(f'/api/posts/{self.post.uuid}/')

        assert request.status_code == 200 and request.json() != empty_response

        # prod retrieve only
        # def test_put_post(self):
        #     data = {'text': 'New text', 'tags': ['newtag']}
        #
        #     request = self.client.put(f'/api/posts/{self.post.uuid}/',
        #                               data=json.dumps(data),
        #                               content_type='application/json')
        #     post = Post.objects.get(uuid=self.post.uuid)
        #
        #     assert request.status_code == 200 and \
        #            post.text == data['text']
        #
        # def test_delete_post(self):
        #     request = self.client.delete(f'/api/posts/{self.post.uuid}/')
        #
        #     with self.assertRaises(Post.DoesNotExist):
        #         Post.objects.get(uuid=self.post.uuid)
