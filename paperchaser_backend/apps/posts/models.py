import ast
import json
import uuid

from django.db import models


class Post(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    publish_date = models.DateTimeField(auto_now=True, unique=True)
    _tags = models.CharField(name='tags', max_length=500, blank=False)
    text = models.TextField(blank=False)
    public = models.BooleanField(default=False)

    class Meta:
        db_table = 'post'
        ordering = ['-publish_date']

    def __eq__(self, other):
        return isinstance(other, self.__class__) and \
               self.tags == other.tags and \
               self.text == other.text

    @property
    def tags(self):
        return self._tags

    @tags.getter
    def tags(self):
        try:
            return json.loads(self._tags)
        except:
            raise PostDeserializeException('can\'t deserialize tags. '
                                           'It is not a list')

    @tags.setter
    def tags(self, x):
        if isinstance(x, str):
            x = "['']" if x == '' else x
            try:
                x = ast.literal_eval(x)
            except:
                x = x.replace(' ', '').split(',')

        if not isinstance(x, list):
            raise ValueError(f'tags should be list')

        if len(x) > 5 or x is None or x == []:
            raise ListFieldValueException(f'length must be on '
                                          f'or less than 6, '
                                          f'got {len(x)} in {x}')
        self._tags = json.dumps(x)


class ListFieldValueException(Exception):
    def __init__(self, message):
        super(Exception, self).__init__(message)


class PostDeserializeException(Exception):
    def __init__(self, message):
        super(Exception, self).__init__(message)
