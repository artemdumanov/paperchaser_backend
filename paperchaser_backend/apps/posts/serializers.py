from rest_framework import serializers

from paperchaser_backend.apps.posts.models import Post


class PostExtendedSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = ('uuid', 'publish_date', 'tags', 'text', 'public')

    uuid = serializers.UUIDField(read_only=True)
    publish_date = serializers.DateTimeField(read_only=True)
    tags = serializers.ListField(child=serializers.CharField(),
                                 required=True)
    text = serializers.CharField(required=True)
    public = serializers.BooleanField(required=True)
